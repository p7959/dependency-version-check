# Dependency Version Check

Service to display newer dependency versions of a Maven project. \
You can use this service as a job in your `.gitlab-ci.yml` to automate the detection of outdated Maven dependencies. \
This service uses the goal "display-dependency-updates" of the "Versions Maven Plugin".

---
# Setting up the default job

Set up a job in the `.gitlab-ci.yml` of your project. The service will run in your job and give you an output of the
status of your dependencies.

in `.gitlab-ci.yml`:
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - dependency-check
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```

In the default setting the service picks out Maven dependencies if it is at least 1 major version behind. \
Also Maven dependencies with e.g. *-RC1, *-Alpha, *-Beta, *-M1, ... are ignored. For more details you can see the file
under `src/main/resources/maven-version-rules.xml`. \
If you want to override this configuration file you can see how to do that further down in this documentation.

---
# Job artifacts

The service will automatically move relevant artifact data to a directory called `artifacts`. \
If you want to use artifacts in your job, all you need to do is pass the `artifacts` directory to your pipeline code
as seen in the code examples.

---
# Configuration of the service

You can configure 3 things when using this service fpr your projects:

1. ***Global Setting*** where you can configure the version setting for all your dependencies
2. ***Dependency Config*** where you can configure the version setting for individual dependencies
3. ***Maven Rules*** where you can configure which types of versions you want to ignore

## Global setting configuration

By default, the setting is 1.0 meaning that the service will consider dependencies as outdated when they are 1 major
version behind. \
Format of the config number: {majorVersionSetting}.{minorVersionSetting}

For example:
- 2.0 checks if there are Maven dependencies that are 2 major versions ahead of yours
- 0.5 checks if there are Maven dependencies that are 5 minor versions ahead of yours
- 1.5 checks if there are Maven dependencies that are 1 major version and 5 minor versions ahead of yours

To configure the global setting in your job, do the following in your `.gitlab-ci.yml`
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - >
          dependency-check
          --dvc.settings.global-setting=2.0
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```
You can of course set any version setting you want here. 2.0 is just an example. Make sure you use the correct format.

## Individual setting configuration

If you want to use individual settings for some of your dependencies you can create a .properties-file and pass
the file path to the pipeline configuration.

Your `.properties file` could look like this:
```
# dependency.properties
# Configuring certain dependencies
de.cluetec.logging--gelf-logging-appender=0.2
com.google.firebase--firebase-admin=2.0
```

Format of a setting is: {groupId}--{artifactId}={majorVersionSetting}.{minorVersionSetting}. \
{majorVersionSetting} and {minorVersionSetting} have to be numerical numbers. \
Make sure to separate the {groupId} and {artifactId} with `--` (2 minus symbols). \
Make sure to separate the {majorVersionSetting} and {minorVersionSetting} with `.` (1 dot).


Then you need to pass your file path in the pipeline in your `.gitlab-ci.yml`:
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - >
          dependency-check
          --dvc.files.dependency-config=$CI_PROJECT_DIR/{your_file_path_here}
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```
Make sure you keep in $CI_PROJECT_DIR and then add the filepath to your config file.

A practical usage of such a configuration could look like this:
`--dvc.files.dependency-config=$CI_PROJECT_DIR/src/main/resources/dependency.properties`

Now the service will check every dependency in the project with the default setting (or your configured global setting
which overrides the default setting), except for the dependencies that you have configured in your .properties file.
The service will use your configured settings only for those.

## Maven rules configuration

The Maven-Versions-Plugin can be configured with a .xml file. With this configuration you can tell the plugin to
ignore certain Maven versions (E.g. release candidates or SNAPSHOTs). \
The service provides a default configuration .xml file, so you don't have to create one yourself. \
You can find the file in this project under: `src/main/resources/maven-version-rules.xml` \
However if you want to override this file and use your own configurations or don't want the default file,
you can create your own .xml file in your project and pass the file path in the pipeline job.

For this configuration you need to give the job an export variable in your `.gitlab-ci.yml`:
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - export RULES_FILE=file://$CI_PROJECT_DIR/{your_file_path_here}
        - dependency-check
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```
Make sure you keep in $CI_PROJECT_DIR and then add the file path to your config file.

A practical usage of such a configuration could look like this:
`export RULES_FILE=file://$CI_PROJECT_DIR/src/main/resources/maven-version-rules.xml`

To configure this file on your own use `src/main/resources/maven-version-rules.xml` as context.

## Using all possible configurations in your job

will look like this in your `.gitlab-ci.yml`:
```
dependency-check:
  stage: dependency-check
  image:
    name: peralty/dependency-versions-check
  script:
    - export RULES_FILE=file://$CI_PROJECT_DIR/{your_file_path_here}
    - >
      dependency-check
      --dvc.settings.global-setting=2.0
      --dvc.files.dependency-config=$CI_PROJECT_DIR/{your_file_path_here}
  artifacts:
     when: always
     paths:
       - artifacts
     expire_in: 1 week
```
Or you can just use the default service as shown at the top of this documentation.

---
# Troubleshooting

This service uses the goal "display-dependency-updates" of the "Versions Maven Plugin" as a starting point. \
This plugin scans the mvnRepository (https://mvnrepository.com). For some dependencies there are wrong
outputs for their newest versions. This happens when they have unusual version names in their history.
The "Versions Maven Plugin" then sometimes picks up the wrong version as newest version.

### Example

The dependency `commons-io:commons-io` is one of those dependencies that does not really work here.
It could be used in the Version `1.4` in your project and the newest Version could be for example
`2.11.0`. However, the  "Versions Maven Plugin" takes `20030203.000550` as newest version since that is
the highest number in the version history
(as you can see here: https://mvnrepository.com/artifact/commons-io/commons-io).

### Solution

Unfortunately the service in this version cannot properly check these dependencies due to the wrong output
of the "Versions Maven Plugin".

To prevent your pipeline from failing due to these wrong outputs I recommend using one of the following workarounds:

### 1. Using .properties file configuration
Take the dependency that is causing the trouble and create a setting in a .properties file (as explained
in the Configuration chapter) high enough so that it is ignored.

Using the Example of the `commons-io:commons-io` Dependency you could do the following:
- create a file `dependency.properties` that could look like this:
```
# Configuring certain dependencies
commons-io--commons-io=300000000.0
```
Notice that the setting of the Dependency is so high that it will not be considered outdated.
- You then need to configure the usage of the file `dependency.properties` in your pipeline `.gitlab-ci.yml`:
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - >
          dependency-check
          --dvc.files.dependency-config=$CI_PROJECT_DIR/{your_file_path_here}
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```

### 2. Using maven rules configuration
Create a rule in your own configuration file for the "Versions Maven Plugin" (as explained
in the Configuration chapter) to ignore the dependencies version in the first place.
- create a configuration file that could be called: `maven-version-rules.xml`
- in a tag <ignoreVersion> configure a fitting rule
- You then need to configure the usage of the file `maven-version-rules.xml` in your pipeline `.gitlab-ci.yml`:
```
dependency-check:
    stage: dependency-check
    image:
        name: peralty/dependency-versions-check
    script:
        - export RULES_FILE=file://$CI_PROJECT_DIR/{your_file_path_here}
        - dependency-check
    artifacts:
       when: always
       paths:
         - artifacts
       expire_in: 1 week
```
---
# Using pipeline with cache

in `.gitlab-ci.yml` with default service:
```
dependency-check:
  stage: dependency-check
  image:
    name: peralty/dependency-versions-check
  script:
    - dependency-check
  artifacts:
    when: always
    paths:
      - artifacts
    expire_in: 1 week
  cache:
    key: dependency-check
    untracked: true
    paths:
      - .m2/repository
```

---
# Contact

When you have any trouble using the service feel free to contact the developer at any time via:
info@perilousroyalty.de
<br><br>