package de.peralty.checkevent.config;

import de.peralty.checkevent.data.Version;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/***
 * Contains the Configuration logic for each configurable setting using Spring Configuration
 */
@Data
@ConfigurationProperties(prefix = "dvc.settings")
public class SettingsConfig {
    private String globalSetting;

    /***
     * Taking the setting and parsing the single values (majorVersionSetting and minorVersionSetting) out of it
     * @return a Version object with its setting values
     */
    public Version parseSetting() {
        var value = getGlobalSetting();
        String[] valueSplit = value.split("\\.");
        if (valueSplit.length != 2) {
            throw new IllegalArgumentException("Wrong format of the version setting");
        }
        int majorVersionSetting = Integer.parseInt(valueSplit[0]);
        int minorVersionSetting = Integer.parseInt(valueSplit[1]);
        String versionName = String.format("%d.%d.%s", majorVersionSetting, minorVersionSetting, "*");
        return new Version(versionName, majorVersionSetting, minorVersionSetting, "*");
    }
}