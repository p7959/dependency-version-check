package de.peralty.checkevent.config;

import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.Version;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.PathResource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/***
 * Contains the Configuration logic for each configurable file using Spring Configuration
 */
@Data
@ConfigurationProperties(prefix = "dvc.files")
public class FilesConfig {
    private PathResource input;
    private PathResource output;
    private PathResource dependencyConfig;
    private Map<MavenCoordinate, Version> map;

    /***
     * Takes a .properties configuration File and creates a Map<MavenCoordinate, Version> from it,
     * that maps the version setting for every MavenCoordinate in the file.
     * @param configFile Input file
     * @return Map<MavenCoordinate, Version>
     * @throws IOException when something IO related goes wrong
     */
    public Map<MavenCoordinate, Version> createMapFromDependencyConfigFile(File configFile) throws IOException {
        Map<MavenCoordinate, Version> propertyMap = new HashMap<>();
        Properties properties = new Properties();
        try (var reader = new FileReader(configFile)) {
            properties.load(reader);
        }
        properties.forEach( (k,v) -> {
            MavenCoordinate mavenCoordinate = MavenCoordinate.createFromPropertiesConfig(k.toString());
            Version versionSetting = Version.create(v.toString());
            versionSetting.setName(versionSetting.getName() + "*");
            propertyMap.put(mavenCoordinate, versionSetting);
        });
        return propertyMap;
    }
}