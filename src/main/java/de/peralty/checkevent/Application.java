package de.peralty.checkevent;

import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.services.DependencyChecker;
import de.peralty.checkevent.services.FileParser;
import de.peralty.checkevent.services.OutputGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import java.util.List;

@AllArgsConstructor
@Slf4j
@SpringBootApplication
@ConfigurationPropertiesScan("de.peralty.checkevent.config")
public class Application implements CommandLineRunner {

    private final FileParser fileParser;
    private final DependencyChecker dependencyChecker;
    private final OutputGenerator outputGenerator;

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<MavenDependency> allDependencies = fileParser.parseFile();
        List<MavenDependency> outdatedDependencies = dependencyChecker.versionCheck(allDependencies);

        if (!(outdatedDependencies.isEmpty())) {
            outputGenerator.generateOutputFile(outdatedDependencies);
            log.error("There are Maven Dependencies in your Project that are " +
                    "out of date. Update the Dependency or change your Dependency Configuration.\n" +
                    "Outdated Dependencies are displayed in this log and in the generated file: 'output.txt'");
            outputGenerator.generateOutputLog(outdatedDependencies);
            System.exit(1);
        } else {
            outputGenerator.generateOutputFile();
            log.info("No Maven-Dependencies that have newer versions have been detected. " +
                    "All Dependencies are up to date according to your set standards.");
        }
    }
}

