package de.peralty.checkevent.data;

import lombok.*;

/***
 * data class to store a full MavenDependency
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MavenDependency {
    private MavenCoordinate mavenCoordinate;
    private Version versionOld;
    private Version versionNew;
}

