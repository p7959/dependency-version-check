package de.peralty.checkevent.data;

import lombok.*;
import org.apache.commons.lang3.StringUtils;
import java.util.stream.IntStream;

/***
 * data class to store a Version of a MavenDependency
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Version implements Comparable<Version> {

    private String name;
    private Integer majorVersion;
    private Integer minorVersion;
    private String lowerVersion;

    /***
     * Taking in a full Version as String from the input text file.
     * Parsing the String and creating a Version object from the data.
     * @param versionString A full Version as String coming from the input text file
     * @return A parsed Version object
     */
    public static Version create(String versionString) {
        String[] versionSplit = versionString.trim().split("\\.");
        int length = versionSplit.length;
        Version returnVersion = new Version();
        returnVersion.setName(versionString.trim());
        IntStream.range(0, length)
                .forEach(index -> {
                    if(index == 0)
                        returnVersion.setMajorVersion(Integer.parseInt(versionSplit[index]));
                    if (index == 1)
                        returnVersion.setMinorVersion(Integer.parseInt(versionSplit[index]));
                    if (index == 2)
                        returnVersion.setLowerVersion(versionSplit[index] + ".");
                    if (index > 2)
                        returnVersion.setLowerVersion(returnVersion.getLowerVersion() + versionSplit[index] + ".");
                });
        if (returnVersion.getLowerVersion() != null) {
            returnVersion.setLowerVersion(StringUtils.chop(returnVersion.getLowerVersion()));
        }
        return returnVersion;
    }

    /***
     * Adding the values of two Version objects.
     * This is used by th DependencyChecker to evaluate if a Dependency is outdated.
     * @param other The other object
     * @return A new Version object that contains
     */
    public Version plus(Version other) {
        Integer majorVersion = this.getMajorVersion() + other.getMajorVersion();
        Integer minorVersion = this.getMinorVersion() + other.getMinorVersion();
        String lowerVersion = "*";
        String name = String.format("%d.%d.%s", majorVersion, minorVersion, lowerVersion);
        return new Version(name, majorVersion, minorVersion, lowerVersion);
    }

    /***
     * @return Output of the object as String
     */
    @Override
    public String toString() {
        return name;
    }

    /***
     * Comparing two Versions
     * @param other other Version
     * @return Integer value representing whether the Versions are equal, smaller or larger
     */
    @Override
    public int compareTo(Version other) {
        if (this.getMajorVersion().equals(other.getMajorVersion()) &&
                this.getMinorVersion().equals(other.getMinorVersion())) {
            return 0;
        }
        if (this.getMajorVersion() > other.getMajorVersion() ||
                (this.getMajorVersion().equals(other.getMajorVersion()) &&
                        this.getMinorVersion() > other.getMinorVersion())) {
            return 1;
        }
        return -1;
    }
}