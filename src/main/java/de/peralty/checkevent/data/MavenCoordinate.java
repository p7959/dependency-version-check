package de.peralty.checkevent.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * data class to store a MavenCoordinate
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MavenCoordinate {
    private String groupId;
    private String artifactId;

    /***
     * Takes a full dependency name as string input from the input text file and creates a MavenCoordinate object to
     * return from parsing the input string
     * @param dependencyName A full dependency name as String
     * @return Object of a MavenCoordinate
     */
    public static MavenCoordinate createFromTextFile(String dependencyName) {
        String groupId = dependencyName.split(":")[0].trim();
        String artifactId = dependencyName.split(":")[1].trim();
        return new MavenCoordinate(groupId, artifactId);
    }

    /***
     * Takes a full dependency name as string input from a properties config file  and creates a
     * MavenCoordinate object to return from parsing the input string.
     * @param coordinateName A full dependency name as String
     * @return Object of a MavenCoordinate
     */
    public static MavenCoordinate createFromPropertiesConfig(String coordinateName) {
        String[] keySplit = coordinateName.split("--");
        if (keySplit.length != 2) {
            throw new IllegalArgumentException("Wrong format of the dependency name");
        }
        String groupId = keySplit[0];
        String artifactId = keySplit[1];
        return new MavenCoordinate(groupId, artifactId);
    }
}
