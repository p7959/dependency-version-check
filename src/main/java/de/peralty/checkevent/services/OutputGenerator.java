package de.peralty.checkevent.services;

import de.peralty.checkevent.config.FilesConfig;
import de.peralty.checkevent.data.MavenDependency;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * This class contains the logic to create an output-file and an output-log that displays
 * all MavenDependency-Objects that have newer major-versions available.
 *
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OutputGenerator {

    private final FilesConfig filesConfig;

    /***
     * Generates an output file from the list of outdated dependencies
     * @param outdatedDependencies A List of MavenDependency-Objects that have newer major-versions
     * @throws IOException for the files
     */
    public void generateOutputFile(List<MavenDependency> outdatedDependencies) throws IOException {
        String filePath = filesConfig.getOutput().getFile().getPath();
        try (var outputWriter = new FileWriter(filePath)) {
            outputWriter.write("The following dependencies have newer versions: \n");
            for (MavenDependency mavenDependency: outdatedDependencies) {
                outputWriter.write("\t" +
                        mavenDependency.getMavenCoordinate().getGroupId() + ":" +
                        mavenDependency.getMavenCoordinate().getArtifactId() + " ......... " +
                        mavenDependency.getVersionOld().getMajorVersion() + "." +
                        mavenDependency.getVersionOld().getMinorVersion()
                );
                if (mavenDependency.getVersionOld().getLowerVersion() != null) {
                    outputWriter.write( "." +
                            mavenDependency.getVersionOld().getLowerVersion()
                    );
                }
                if (mavenDependency.getVersionNew().getLowerVersion() == null) {
                    outputWriter.write(" -> " +
                            mavenDependency.getVersionNew().getMajorVersion() + "." +
                            mavenDependency.getVersionNew().getMinorVersion() + "\n"
                    );
                } else {
                    outputWriter.write(" -> " +
                            mavenDependency.getVersionNew().getMajorVersion() + "." +
                            mavenDependency.getVersionNew().getMinorVersion() + "." +
                            mavenDependency.getVersionNew().getLowerVersion() + "\n"
                    );
                }
            }
        }
    }

    /***
     * Generates an output file for the case that no outdated dependencies have been found
     * @throws IOException for the files
     */
    public void generateOutputFile() throws IOException {
        String filePath = filesConfig.getOutput().getFile().getPath();
        try (var outputWriter = new FileWriter(filePath)) {
            outputWriter.write("No Maven-Dependencies that have newer versions have been detected.\n" +
                    "All Dependencies are up to date according to your set standards.");
        }
    }

    /**
     * Generates an output-log
     * @param outdatedDependencies A List of MavenDependency-Objects that have newer major-versions
     */
    public void generateOutputLog(List<MavenDependency> outdatedDependencies) {
        for (MavenDependency mavenDependency : outdatedDependencies) {
            String outputLog = "Outdated dependency: " +
                    mavenDependency.getMavenCoordinate().getGroupId() + ":" +
                    mavenDependency.getMavenCoordinate().getArtifactId() + " ......... " +
                    mavenDependency.getVersionOld().getMajorVersion() + "." +
                    mavenDependency.getVersionOld().getMinorVersion();
            if (mavenDependency.getVersionOld().getLowerVersion() != null) {
                outputLog += ("." + mavenDependency.getVersionOld().getLowerVersion());
            }
            if (mavenDependency.getVersionNew().getLowerVersion() == null) {
                outputLog += (" -> " + mavenDependency.getVersionNew().getMajorVersion() + "." +
                        mavenDependency.getVersionNew().getMinorVersion());
            } else {
                outputLog += (" -> " + mavenDependency.getVersionNew().getMajorVersion() + "." +
                        mavenDependency.getVersionNew().getMinorVersion() + "." +
                        mavenDependency.getVersionNew().getLowerVersion());
            }
            log.error(outputLog);
        }
    }
}

