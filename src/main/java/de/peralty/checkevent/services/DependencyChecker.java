package de.peralty.checkevent.services;

import de.peralty.checkevent.config.FilesConfig;
import de.peralty.checkevent.config.SettingsConfig;
import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.data.Version;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.peralty.checkevent.services.DependencyChecker.SettingType.*;

/**
 *  This class contains the logic to compare the
 *  current version of the MavenDependencies to their newer version.
 *
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class DependencyChecker {

    private final SettingsConfig settingsConfig;
    private final FilesConfig filesConfig;

    /***
     * Takes the full list of data from the input text file.
     * Checks each MavenDependency and evaluates whether it is outdated or not.
     * The settings decide whether a dependency is outdated or not.
     * Adds outdated dependencies to the output list and returns that.
     * @param allDependencies List of all MavenDependencies coming from the data of the input text file.
     * @return A List of outdated dependencies as the user sets the configuration for his dependencies.
     * @throws IOException when something IO related goes wrong
     */
    public List<MavenDependency> versionCheck(List<MavenDependency> allDependencies) throws IOException {
        List<MavenDependency> outdatedDependencies = new ArrayList<>();
        File configFile = filesConfig.getDependencyConfig().getFile();
        Map<MavenCoordinate, Version> map = filesConfig.createMapFromDependencyConfigFile(configFile);

        for (MavenDependency mavenDependency : allDependencies) {
            MavenCoordinate mavenCoordinate = mavenDependency.getMavenCoordinate();
            Version settingVersion = map.getOrDefault(mavenCoordinate, settingsConfig.parseSetting());
//            log.info("Setting for " + mavenCoordinate + ": " + settingVersion); // <- for dev/debug only!
            Version versionOld = mavenDependency.getVersionOld();
            Version versionNew = mavenDependency.getVersionNew();
            Version sum = versionOld.plus(settingVersion);
            boolean addToList = isDependencyOutdated(settingVersion, sum, versionNew);
            if (addToList) {
                outdatedDependencies.add(mavenDependency);
            }
        }
        return outdatedDependencies;
    }

    /* Helper Methods */
    /***
     * Checks if the Dependency is outdated or not depending on the setting
     * @param settingVersion the setting
     * @param sum A computed version to check if it is outdated
     * @return whether the dependency is outdated (true) or not (false)
     */
    private boolean isDependencyOutdated(Version settingVersion, Version sum, Version versionNew) {
        var settingType = getSettingType(settingVersion);
        switch (settingType) {
            case MAJOR_ONLY:
                if (versionNew.getMajorVersion() >= sum.getMajorVersion())
                    return true;
                break;
            case MINOR_ONLY:
                if (sum.getMajorVersion() > versionNew.getMajorVersion())
                    return false;
                if (sum.getMajorVersion() < versionNew.getMajorVersion())
                    return true;
                if (sum.getMinorVersion() <= versionNew.getMinorVersion())
                    return true;
                break;
            case MAJOR_AND_MINOR:
                if (sum.compareTo(versionNew) <= 0)
                    return true;
                break;
            case ZERO:
                return versionNew.compareTo(sum) > 0;
        }
        return false;
    }

    /***
     * returns an Enum-Type that represents the composition of the Setting.
     * This helps the evaluation of the status of a Maven Dependency for it decreases the
     * complexity.
     * @param settingVersion The respective version setting for a MavenDependency
     * @return An Enum-Type that represents the composition of the Setting
     */
    private SettingType getSettingType (Version settingVersion) {
        if ((settingVersion.getMajorVersion() != 0) && (settingVersion.getMinorVersion() == 0)) {
            return MAJOR_ONLY;
        }
        if ((settingVersion.getMajorVersion() == 0) && (settingVersion.getMinorVersion() != 0)) {
            return MINOR_ONLY;
        }
        if ((settingVersion.getMajorVersion() != 0) && (settingVersion.getMinorVersion() != 0)) {
            return MAJOR_AND_MINOR;
        }
        return ZERO;
    }

    /***
     * Enum-Types that represent the composition of the Setting
     */
    protected enum SettingType {
        MAJOR_ONLY,
        MINOR_ONLY,
        MAJOR_AND_MINOR,
        ZERO
    }
}