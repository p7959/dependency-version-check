package de.peralty.checkevent.services;

import de.peralty.checkevent.config.FilesConfig;
import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.data.Version;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * This class contains the logic to parse the Input-File "outdated-dependencies.txt"
 *
 */
@Component
@RequiredArgsConstructor
public class FileParser {

    private final FilesConfig filesConfig;

    /***
     * Takes in the input text file created from the maven-versions-plugin.
     * Parsing the file and creating a List of MavenDependency-Objects from it.
     * @return A List of all MavenDependency-Objects created from the input File.
     * @throws IOException when something IO related goes wrong
     */
    public List<MavenDependency> parseFile() throws IOException {
        List<MavenDependency> dependencies = new ArrayList<>();
        final String[] splitDependencyName = {""};
        String filePath = filesConfig.getInput().getFile().getPath();
        Stream<String> lines = Files.lines(Path.of(filePath));
        lines.forEach(line -> {
            if (line.contains("...") && line.contains("->")) {
                String[] courseSplit = line.trim().split("\\.\\.\\.*");
                String dependencyName = courseSplit[0];
                String leftSideVersion = courseSplit[1].split("->")[0].trim();
                String rightSideVersion = courseSplit[1].split("->")[1].trim();
                Version versionOld = Version.create(leftSideVersion);
                Version versionNew = Version.create(rightSideVersion);
                MavenCoordinate coordinate = MavenCoordinate.createFromTextFile(dependencyName);
                MavenDependency parsed = new MavenDependency(coordinate, versionOld, versionNew);
                dependencies.add(parsed);
            }

            if (line.contains("...") && !(line.contains("->"))) {
                String[] split = line.trim().split("\\.\\.\\.*");
                splitDependencyName[0] = split[0];
            }

            if (!(line.contains("...")) && line.contains("->")) {
                String[] split = line.trim().split("->");
                String leftSideVersion = split[0];
                String rightSideVersion = split[1].trim();
                Version versionOld = Version.create(leftSideVersion);
                Version versionNew = Version.create(rightSideVersion);
                MavenCoordinate coordinate = MavenCoordinate.createFromTextFile(splitDependencyName[0]);
                MavenDependency parsed = new MavenDependency(coordinate, versionOld, versionNew);
                dependencies.add(parsed);
            }
        });
        return  dependencies;
    }
}