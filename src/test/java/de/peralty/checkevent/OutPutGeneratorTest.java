//package de.peralty.checkevent;
//
//import ch.qos.logback.classic.Level;
//import ch.qos.logback.classic.Logger;
//import ch.qos.logback.classic.spi.ILoggingEvent;
//import ch.qos.logback.core.read.ListAppender;
//import de.peralty.checkevent.config.FilesConfig;
//import de.peralty.checkevent.data.MavenCoordinate;
//import de.peralty.checkevent.data.MavenDependency;
//import de.peralty.checkevent.data.Version;
//import de.peralty.checkevent.services.OutputGenerator;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.io.FileUtils;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.extension.ExtensionContext;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.ArgumentsProvider;
//import org.junit.jupiter.params.provider.ArgumentsSource;
//import org.slf4j.LoggerFactory;
//import org.springframework.core.io.PathResource;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Stream;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
//
//@Slf4j
//class OutputGeneratorTest {
//    private FilesConfig filesConfig;
//    private OutputGenerator outputGenerator;
//
//    @BeforeEach
//    void init() {
//        filesConfig = mock(FilesConfig.class);
//        outputGenerator = new OutputGenerator(filesConfig);
//    }
//
//    @Nested
//    @DisplayName("Testing the method \"generateOutputFile\"")
//    class GenerateOutputFileTests {
//        @Nested
//        @DisplayName("Correct data input should be handled correctly")
//        class SuccessCases {
//
//            @ParameterizedTest
//            @ArgumentsSource(OutputArgumentsProviderSuccess.class)
//            @DisplayName("Method \"generateOutputFile\" should generate the correct contents in the " +
//                    "output file from the list of outdated dependencies")
//            void generateOutputFileTestCorrectInput(String expectedFilePath,
//                                                    List<MavenDependency> outdatedDependencies) throws IOException {
//                PathResource providedPathResource =
//                        new PathResource("./src/test/resources/outputGeneratorTest/providedOutputFile/output.txt");
//                when(filesConfig.getOutput()).thenReturn(providedPathResource);
//                File expectedFile = new File(expectedFilePath);
//                File providedFile = new File(filesConfig.getOutput().getFile().getPath());
//                outputGenerator.generateOutputFile(outdatedDependencies);
//
//                assertEquals (
//                        FileUtils.readFileToString(expectedFile, "utf-8")
//                                .replaceAll("[ \t\r\n]", ""),
//                        FileUtils.readFileToString(providedFile, "utf-8")
//                                .replaceAll("[ \t\r\n]", "")
//                );
//            }
//
//        }
//
//        @Nested
//        @DisplayName("Wrong data input should generate a failure")
//        class FailureCases {
//
//            @ParameterizedTest
//            @ArgumentsSource(OutputArgumentsProviderFailure.class)
//            @DisplayName("The generated output file from method \"generateOutputFile\" should not be computed " +
//                    "as being identical to a file with unidentical contents")
//            void generateFileTestWrongInput(String expectedFilePath,
//                                            List<MavenDependency> outdatedDependencies) throws IOException {
//                PathResource providedPathResource =
//                        new PathResource("./src/test/resources/outputGeneratorTest/providedOutputFile/output.txt");
//                when(filesConfig.getOutput()).thenReturn(providedPathResource);
//                File expectedFile = new File(expectedFilePath);
//                File providedFile = new File(filesConfig.getOutput().getFile().getPath());
//                outputGenerator.generateOutputFile(outdatedDependencies);
//                boolean outcome =
//                        FileUtils.readFileToString(expectedFile, "utf-8")
//                                .replaceAll("[ \t\r\n]", "")
//                                .equals(FileUtils.readFileToString(providedFile, "utf-8")
//                                        .replaceAll("[ \t\r\n]", ""));
//                assertFalse(outcome);
//            }
//
//        }
//    }
//
//    @Nested
//    @DisplayName("Testing the method \"generateOutputLog\"")
//    class GenerateOutputLogTests {
//
//        @Nested
//        @DisplayName("Correct data input should be handled correctly")
//        class SuccessCases {
//
//            @ParameterizedTest
//            @ArgumentsSource(LogArgumentsProvider.class)
//            @DisplayName("Method \"generateOutputLog\" should generate the correct contents in the " +
//                    "log from the list of outdated dependencies")
//            void generateOutputLogTestCorrectInput(String expected, List<MavenDependency> outdatedDependencies) {
//                List<ILoggingEvent> logList = createLoggerData(outdatedDependencies);
//
//                assertAll(
//                        () -> assertEquals(expected.replaceAll(" ", ""),
//                                logList.get(0).getMessage().replaceAll(" ", "")),
//                        () -> assertEquals(Level.ERROR, logList.get(0).getLevel())
//                );
//            }
//
//        }
//
//        @Nested
//        @DisplayName("Wrong data input should generate a failure")
//        class FailureCases {
//
//            @ParameterizedTest
//            @ArgumentsSource(LogArgumentsProviderUnidentical.class)
//            @DisplayName("The generated output log from the method \"generateOutputLog\" should not be " +
//                    "recognized as being identical compared to an unidentical log")
//            void generateOutputLogTestCorrectInput(String expected, List<MavenDependency> outdatedDependencies) {
//                List<ILoggingEvent> logList = createLoggerData(outdatedDependencies);
//
//                boolean outcome = expected.replaceAll(" ", "")
//                        .equals(logList.get(0).getMessage().replaceAll(" ", ""));
//                assertFalse(outcome);
//            }
//
//            @ParameterizedTest
//            @ArgumentsSource(LogArgumentsProviderLogLevel.class)
//            @DisplayName("Generated output Log should be incorrect if the log level is different than expected")
//            void generateOutputLogWrongLogLevel(List<MavenDependency> outdatedDependencies) {
//                List<ILoggingEvent> logList = createLoggerData(outdatedDependencies);
//
//                boolean outcomeDebug = Level.DEBUG.equals(logList.get(0).getLevel());
//                boolean outcomeError = Level.INFO.equals(logList.get(0).getLevel());
//                boolean outcomeWarn = Level.WARN.equals(logList.get(0).getLevel());
//                boolean outcomeTrace = Level.TRACE.equals(logList.get(0).getLevel());
//                boolean outcomeOff = Level.OFF.equals(logList.get(0).getLevel());
//                boolean outcomeAll = Level.ALL.equals(logList.get(0).getLevel());
//
//                assertAll(
//                        () -> assertFalse(outcomeDebug),
//                        () -> assertFalse(outcomeError),
//                        () -> assertFalse(outcomeWarn),
//                        () -> assertFalse(outcomeTrace),
//                        () -> assertFalse(outcomeOff),
//                        () -> assertFalse(outcomeAll)
//                );
//            }
//
//        }
//    }
//
//    private static class OutputArgumentsProviderSuccess implements ArgumentsProvider {
//        @Override
//        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
//            // inputList 1
//            List<MavenDependency> inputList1 = new ArrayList<>();
//            Version v1Old = new Version("7.1.1",7, 1, "1");
//            Version v1New = new Version("8.1.0",8, 1, "0");
//            MavenCoordinate mc1 = new MavenCoordinate("com.google.firebase", "firebase-admin");
//            MavenDependency mavenDependency1 =
//                    new MavenDependency(mc1, v1Old, v1New);
//            inputList1.add(mavenDependency1);
//
//            // inputList 2
//            List<MavenDependency> inputList2 = new ArrayList<>();
//            Version v2Old = new Version("1.5",1, 5, null);
//            Version v2New = new Version("2.9.0",2, 9, "0");
//            MavenCoordinate mc2 = new MavenCoordinate("org.apache.commons", "commons-csv");
//            MavenDependency mavenDependency2 =
//                    new MavenDependency(mc2, v2Old, v2New);
//            inputList2.add(mavenDependency2);
//
//            return Stream.of(
//                    Arguments.of("./src/test/resources/outputGeneratorTest/expectedOutputFiles/expectedOutput1.txt",
//                            inputList1),
//                    Arguments.of("./src/test/resources/outputGeneratorTest/expectedOutputFiles/expectedOutput3.txt",
//                            inputList2)
//            );
//        }
//    }
//
//    private static class OutputArgumentsProviderFailure implements ArgumentsProvider {
//        @Override
//        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
//            // inputList 1
//            List<MavenDependency> inputList1 = new ArrayList<>();
//            Version v1Old = new Version("7.1.1",7, 1, "1");
//            Version v1New = new Version("8.1.0",8, 1, "0");
//            MavenCoordinate mc1 = new MavenCoordinate("com.google.firebase", "firebase-admin");
//            MavenDependency mavenDependency1 =
//                    new MavenDependency(mc1, v1Old, v1New);
//            inputList1.add(mavenDependency1);
//
//            return Stream.of(
//                    Arguments.of("./src/test/resources/outputGeneratorTest/expectedOutputFiles/expectedOutput2.txt",
//                            inputList1)
//            );
//        }
//    }
//
//    private static class LogArgumentsProvider implements ArgumentsProvider {
//        @Override
//        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
//
//            // inputList 1
//            List<MavenDependency> inputList1 = new ArrayList<>();
//            Version v1Old = new Version("1.5",1, 5, null);
//            Version v1New = new Version("2.9.0",2, 9, "0");
//            MavenCoordinate mc1 = new MavenCoordinate("org.apache.commons", "commons-csv");
//            MavenDependency mavenDependency1 =
//                    new MavenDependency(mc1, v1Old, v1New);
//            inputList1.add(mavenDependency1);
//            // expected log message 1
//            String expected1 = "Outdated dependency: org.apache.commons:commons-csv ......... 1.5 -> 2.9.0";
//
//            return Stream.of(
//                    Arguments.of(expected1, inputList1)
//            );
//        }
//    }
//
//    private static class LogArgumentsProviderUnidentical implements ArgumentsProvider {
//        @Override
//        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
//            // inputList 1
//            List<MavenDependency> inputList1 = new ArrayList<>();
//            Version v1Old = new Version("1.5",1, 5, null);
//            Version v1New = new Version("2.9.0",2, 9, "0");
//            MavenCoordinate mc1 = new MavenCoordinate("org.apache.commons", "commons-csv");
//            MavenDependency mavenDependency1 =
//                    new MavenDependency(mc1, v1Old, v1New);
//            inputList1.add(mavenDependency1);
//            // wrong log message 1
//            String wrongLog1 = "Outdated dependency: org.apache.commons:commons-csv ......... 1.5.null -> 2.9.0";
//
//            // inputList 2
//            List<MavenDependency> inputList2 = new ArrayList<>();
//            Version v2Old = new Version("7.1.1",7, 1, "1");
//            Version v2New = new Version("8.1.0",8, 1, "0");
//            MavenCoordinate mc2 = new MavenCoordinate("com.google.firebase", "firebase-admin");
//            MavenDependency mavenDependency2 =
//                    new MavenDependency(mc2, v2Old, v2New);
//            inputList2.add(mavenDependency2);
//            // wrong log messages 2-14 comparing to inputList2
//            String wrongLog2 = "Outdated dependency: com.google.firebase:firebase-admin ......... 7.1.1 -> 8.1.9";
//            String wrongLog3 = "Outdated dependency: com.google.firebase:firebase-admin ......... 7.1.1 -> 8.9.0";
//            String wrongLog4 = "Outdated dependency: com.google.firebase:firebase-admin ......... 7.1.1 -> 9.1.0";
//            String wrongLog5 = "Outdated dependency: com.google.firebase:firebase-admin ......... 7.1.9 -> 8.1.0";
//            String wrongLog6 = "Outdated dependency: com.google.firebase:firebase-admin ......... 7.9.1 -> 8.1.0";
//            String wrongLog7 = "Outdated dependency: com.google.firebase:firebase-admin ......... 9.1.1 -> 8.1.0";
//            String wrongLog8 = "Outdated dependency: com.google.firebase:firebase-admin 7.1.1 -> 8.1.9";
//            String wrongLog9 = "Outdated dependency: com.google.firebase:something-else ......... 7.1.1 -> 8.1.0";
//            String wrongLog10 = "Outdated dependency: something-else:firebase-admin ......... 7.1.1 -> 8.1.0";
//            String wrongLog11 = "Something Else: com.google.firebase:firebase-admin ......... 7.1.1 -> 8.1.0";
//            String wrongLog12 = "com.google.firebase:firebase-admin ......... 7.1.1 -> 8.1.0";
//            String wrongLog13 = "Outdated dependency: com.google.firebasefirebase-admin ......... 7.1.1 -> 8.1.0";
//            String wrongLog14 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam";
//
//            return Stream.of(
//                    Arguments.of(wrongLog1, inputList1),
//                    Arguments.of(wrongLog2, inputList2),
//                    Arguments.of(wrongLog3, inputList2),
//                    Arguments.of(wrongLog4, inputList2),
//                    Arguments.of(wrongLog5, inputList2),
//                    Arguments.of(wrongLog6, inputList2),
//                    Arguments.of(wrongLog7, inputList2),
//                    Arguments.of(wrongLog8, inputList2),
//                    Arguments.of(wrongLog9, inputList2),
//                    Arguments.of(wrongLog10, inputList2),
//                    Arguments.of(wrongLog11, inputList2),
//                    Arguments.of(wrongLog12, inputList2),
//                    Arguments.of(wrongLog13, inputList2),
//                    Arguments.of(wrongLog14, inputList2)
//            );
//        }
//    }
//
//    private static class LogArgumentsProviderLogLevel implements ArgumentsProvider {
//        @Override
//        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
//            // inputList 1
//            List<MavenDependency> inputList1 = new ArrayList<>();
//            Version v1Old = new Version("1.5",1, 5, null);
//            Version v1New = new Version("2.9.0",2, 9, "0");
//            MavenCoordinate mc1 = new MavenCoordinate("org.apache.commons", "commons-csv");
//            MavenDependency mavenDependency1 =
//                    new MavenDependency(mc1, v1Old, v1New);
//            inputList1.add(mavenDependency1);
//
//            // inputList 2
//            List<MavenDependency> inputList2 = new ArrayList<>();
//            Version v2Old = new Version("7.1.1",7, 1, "1");
//            Version v2New = new Version("8.1.0",8, 1, "0");
//            MavenCoordinate mc2 = new MavenCoordinate("com.google.firebase", "firebase-admin");
//            MavenDependency mavenDependency2 =
//                    new MavenDependency(mc2, v2Old, v2New);
//            inputList2.add(mavenDependency2);
//
//            return Stream.of(
//                    Arguments.of(inputList1),
//                    Arguments.of(inputList2)
//            );
//        }
//    }
//
//    private List<ILoggingEvent> createLoggerData(List<MavenDependency> outdatedDependencies) {
//        Logger logger = (Logger) LoggerFactory.getLogger(OutputGenerator.class);
//        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
//        listAppender.start();
//        logger.addAppender(listAppender);
//        outputGenerator.generateOutputLog(outdatedDependencies);
//        return listAppender.list;
//    }
//}
//
