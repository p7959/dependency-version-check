package de.peralty.checkevent;

import com.github.stefanbirkner.systemlambda.SystemLambda;
import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.data.Version;
import de.peralty.checkevent.services.DependencyChecker;
import de.peralty.checkevent.services.FileParser;
import de.peralty.checkevent.services.OutputGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

class ApplicationTest {

    private Application application;
    private FileParser fileParser;
    private DependencyChecker dependencyChecker;
    private OutputGenerator outputGenerator;

    @BeforeEach
    void init() {
        fileParser = mock(FileParser.class);
        dependencyChecker = mock(DependencyChecker.class);
        outputGenerator = mock(OutputGenerator.class);
        application = new Application(fileParser, dependencyChecker, outputGenerator);
    }

    @Test
    @DisplayName("Should call all the correct methods when List of outdated Dependencies is empty")
    void emptyOutdatedDependenciesList() throws Exception {
        when(fileParser.parseFile()).thenReturn(new ArrayList<>());
        when(dependencyChecker.versionCheck(anyList())).thenReturn(Collections.emptyList());

        application.run();

        verify(outputGenerator).generateOutputFile();
        verify(outputGenerator, never()).generateOutputFile(anyList());
        verify(outputGenerator, never()).generateOutputLog(anyList());
    }

    @Test
    @DisplayName("Should call all the correct methods when List of outdated Dependencies is empty")
    void outdatedDependenciesList() throws Exception {
        List<MavenDependency> parserList = new ArrayList<>();
        List<MavenDependency> checkerList = new ArrayList<>();
        Version v1Old = new Version("1.18.18", 1,18,"18");
        Version v1New = new Version("2.23.20",2,23,"20");
        MavenCoordinate mc1 = new MavenCoordinate("org.projectlombok", "lombok");
        MavenDependency mavenDependency1 =
                new MavenDependency(mc1, v1Old, v1New);
        parserList.add(mavenDependency1);
        checkerList.add(mavenDependency1);

        when(fileParser.parseFile()).thenReturn(parserList);
        when(dependencyChecker.versionCheck(anyList())).thenReturn(checkerList);

        SystemLambda.catchSystemExit(() -> {
            application.run();
        });
        verify(outputGenerator).generateOutputFile(anyList());
        verify(outputGenerator).generateOutputLog(anyList());
        verify(outputGenerator, never()).generateOutputFile();
    }

}