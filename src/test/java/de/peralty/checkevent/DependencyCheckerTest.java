package de.peralty.checkevent;

import de.peralty.checkevent.config.FilesConfig;
import de.peralty.checkevent.config.SettingsConfig;
import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.data.Version;
import de.peralty.checkevent.services.DependencyChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.core.io.PathResource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DependencyCheckerTest {

    private DependencyChecker dependencyChecker;
    private SettingsConfig settingsConfig;
    private FilesConfig filesConfig;

    @BeforeEach
    void init() {
        settingsConfig = mock(SettingsConfig.class);
        filesConfig = mock(FilesConfig.class);
        dependencyChecker = new DependencyChecker(settingsConfig, filesConfig);
    }

    @Nested
    @DisplayName("(SuccessCases) Dependencies that are outdated should be added to the output list correctly")
    class SuccessCases {

        @Nested
        @DisplayName("Standard configuration of 1.0 (=Major only Setting)")
        class StandardConfigurationMajorOnly {

            @ParameterizedTest
            @ArgumentsSource(SuccessInputArgumentsProviderProperties.class)
            @DisplayName("Should add dependencies that are considered as outdated to the output list")
            void propertiesCheckSuccessInputNoConfig(List<MavenDependency> inputList, List<MavenDependency> expected)
                    throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "1.0";
                Version settingVersion = new Version("1.0.*", 1, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Major Version only Setting")
        class MajorVersionOnlyConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MajorOnlySuccessArgumentsProvider.class)
            @DisplayName("Should add dependencies that are considered as outdated (with major version only setting " +
                    "that is different from the standard configuration) to the output list")
            void propertiesCheckSuccessInputMajorVersionConfig(List<MavenDependency> inputList,
                                                               List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "2.0";
                Version settingVersion = new Version("2.0.*", 2, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Minor Version only Setting")
        class MinorVersionOnlyConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MinorOnlySuccessArgumentsProvider.class)
            @DisplayName("Should add dependencies that are considered as outdated (with minor version only setting " +
                    "to the output list")
            void propertiesCheckSuccessInputMinorVersionConfig(List<MavenDependency> inputList,
                                                               List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "0.5";
                Version settingVersion = new Version("0.5.*", 0, 5, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Major Version and Minor Version combined Setting")
        class MajorVersionAndMinorVersionConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MajorAndMinorSuccessArgumentsProvider.class)
            @DisplayName("Should add dependencies that are considered as outdated (with major version and minor " +
                    "version setting to the output list")
            void propertiesCheckSuccessInputMajorAndMinorVersionConfig(List<MavenDependency> inputList,
                                                                       List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "1.5";
                Version settingVersion = new Version("1.5.*", 1, 5, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Setting of Zero (0.0)")
        class ZeroConfiguration {

            @ParameterizedTest
            @ArgumentsSource(ZeroSuccessArgumentsProvider.class)
            @DisplayName("Should add dependencies that are considered as outdated (with configuration of Zero (0.0) " +
                    "to the output list")
            void propertiesCheckSuccessInputZeroConfig(List<MavenDependency> inputList,
                                                       List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "0.0";
                Version settingVersion = new Version("0.0.*", 0, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Overriding of Configuration via a .properties file")
        class PropertyFileConfiguration {

            @ParameterizedTest
            @ArgumentsSource(PropertiesFileSuccessArgumentsProvider.class)
            @DisplayName("Should add dependencies that are considered as outdated (with configuration in a " +
                    ".properties file to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource pathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/input1_valid.properties");
                String globalSetting = "1.0";
                Version settingVersion = new Version("1.0.*", 1, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(pathResource);
                when(filesConfig.createMapFromDependencyConfigFile(pathResource.getFile())).thenCallRealMethod();
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }
    }

    @Nested
    @DisplayName("(FailureCases) Dependencies that are not outdated should not be added to the output list")
    class FailureCases {

        @Nested
        @DisplayName("Standard configuration of 1.0 (=Major only Setting)")
        class StandardConfigurationMajorOnly {
            @ParameterizedTest
            @ArgumentsSource(FailureInputArgumentsProviderProperties.class)
            @DisplayName("Should not add dependencies that are not considered as outdated to the output list")
            void propertiesCheckFailureInput(List<MavenDependency> inputList, List<MavenDependency> expected)
                    throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "1.0";
                Version settingVersion = new Version("1.0.*", 1, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Major Version only Setting")
        class MajorVersionOnlyConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MajorOnlyFailureArgumentsProvider.class)
            @DisplayName("Should not add dependencies that are not considered as outdated (with major version only " +
                    "setting that is different from the standard configuration) to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "2.0";
                Version settingVersion = new Version("2.0.*", 2, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Minor Version only Setting")
        class MinorVersionOnlyConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MinorOnlyFailureArgumentsProvider.class)
            @DisplayName("Should not add dependencies that are not considered as outdated (with minor version only " +
                    "setting) to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "0.5";
                Version settingVersion = new Version("0.5.*", 0, 5, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Major Version and Minor Version combined Setting")
        class MajorVersionAndMinorVersionConfiguration {

            @ParameterizedTest
            @ArgumentsSource(MajorAndMinorFailureArgumentsProvider.class)
            @DisplayName("Should not add dependencies that are not considered as outdated (with major version and " +
                    "minor version setting) to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "1.5";
                Version settingVersion = new Version("1.5.*", 1, 5, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Setting of Zero (0.0)")
        class ZeroConfiguration {

            @ParameterizedTest
            @ArgumentsSource(ZeroFailureArgumentsProvider.class)
            @DisplayName("Should not add dependencies that are not considered as outdated (with configuration of " +
                    "Zero (0.0) to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource emptyPathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/noConfig.properties");
                String globalSetting = "0.0";
                Version settingVersion = new Version("0.0.*", 0, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(emptyPathResource);
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }

        @Nested
        @DisplayName("Overriding of Configuration via a .properties file")
        class PropertyFileConfiguration {

            @ParameterizedTest
            @ArgumentsSource(PropertiesFileFailureArgumentsProvider.class)
            @DisplayName("Should not add dependencies that are not considered as outdated (with configuration in a " +
                    ".properties file to the output list")
            void propertiesCheckFailureInputMajorVersionSetting(List<MavenDependency> inputList,
                                                                List<MavenDependency> expected) throws IOException {
                PathResource pathResource =
                        new PathResource("./src/test/resources/dependencyCheckerTest/input1_valid.properties");
                String globalSetting = "1.0";
                Version settingVersion = new Version("1.0.*", 1, 0, "*");
                when(settingsConfig.getGlobalSetting()).thenReturn(globalSetting);
                when(filesConfig.getDependencyConfig()).thenReturn(pathResource);
                when(filesConfig.createMapFromDependencyConfigFile(pathResource.getFile())).thenCallRealMethod();
                when(settingsConfig.parseSetting()).thenReturn(settingVersion);
                List<MavenDependency> provided = dependencyChecker.versionCheck(inputList);
                assertEquals(expected, provided);
            }

        }
    }

    /**
     * This class provides test data for the test method: parseFileExtraLinesTest(...)
     */
    private static class SuccessInputArgumentsProviderProperties implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // String inputFile1 = "./src/test/resources/dependencyCheckerTest/input1_valid.properties";
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("7.1.1", 7,1,"1");
            Version v1New = new Version("8.0.1",8,0,"1");
            MavenCoordinate mc1 = new MavenCoordinate("com.google.firebase", "firebase-admin");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);

            // Test resources 2
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("1.18.18", 1,18,"18");
            Version v2New = new Version("3.22.20",3,22,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();
            expected2.add(mavenDependency2);

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2)
            );
        }
    }

    private static class FailureInputArgumentsProviderProperties implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version is equal; Minor Version is lower
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("7.1.1", 7,1,"1");
            Version v1New = new Version("7.0.1",7,0,"1");
            MavenCoordinate mc1 = new MavenCoordinate("com.google.firebase", "firebase-admin");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();

            // Test resources 2
            // Major Version is lower; Minor Version is higher
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("1.18.18", 1,18,"18");
            Version v2New = new Version("0.22.20",0,22,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();

            // Test resources 2
            // Both versions are equal
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("1.18.18", 1,18,"18");
            Version v3New = new Version("1.18.18",1,18,"18");
            MavenCoordinate mc3 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3)
            );
        }
    }

    private static class MajorOnlySuccessArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version is 2 Versions higher; just in Range of the Configuration (2.0)
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("44.7.24",44,7,"24");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);

            // Test resources 2
            // Major Version is 3 Versions higher; in Range of the Configuration (2.0)
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("45.7.24",44,7,"24");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();
            expected2.add(mavenDependency2);

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2)
            );
        }
    }

    private static class MajorOnlyFailureArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version is Equal
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("42.7.24",42,7,"24");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();

            // Test resources 2
            // Major Version is only 1 higher but should be at least two with provided Configuration
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("43.7.24",43,7,"24");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();

            // Test resources 2
            // Major Version is lower
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("42.2.19", 42,2,"19");
            Version v3New = new Version("40.7.24",40,7,"24");
            MavenCoordinate mc3 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2)
            );
        }
    }

    private static class MinorOnlySuccessArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Equal Major Versions; Minor Version just in range for Setting
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("1.18.18", 1,18,"18");
            Version v1New = new Version("1.23.20",1,23,"20");
            MavenCoordinate mc1 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);

            // Test resources 2
            // Higher Major Version; Minor Version Setting just in range
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("1.18.18", 1,18,"18");
            Version v2New = new Version("2.22.20",2,22,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();
            expected2.add(mavenDependency2);

            // Test resources 3
            // Higher Major Version; Minor Version lower than Setting range
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("1.18.18", 1,18,"18");
            Version v3New = new Version("2.4.20",2,4,"20");
            MavenCoordinate mc3 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();
            expected3.add(mavenDependency3);

            // Test resources 4
            // Equal Major Version; Minor Version far over the range of the Setting
            List<MavenDependency> inputList4 = new ArrayList<>();
            Version v4Old = new Version("1.18.18", 1,18,"18");
            Version v4New = new Version("1.36.2",1,36,"2");
            MavenCoordinate mc4 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            inputList4.add(mavenDependency4);
            List<MavenDependency> expected4 = new ArrayList<>();
            expected4.add(mavenDependency4);

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3),
                    Arguments.of(inputList4, expected4)
            );
        }
    }

    private static class MinorOnlyFailureArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Equal Major Versions; Minor Version just out of range for Setting
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("2.18.18", 2,18,"18");
            Version v1New = new Version("2.22.20",2,22,"20");
            MavenCoordinate mc1 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();

            // Test resources 2
            // Equal Major Versions; Minor Version far out of range for Setting
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("2.18.18", 2,18,"18");
            Version v2New = new Version("2.18.20",2,18,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();

            // Test resources 3
            // Lower Major Version; Minor Version in Setting range
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("2.18.18", 2,18,"18");
            Version v3New = new Version("1.25.2",1,25,"2");
            MavenCoordinate mc3 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();

            // Test resources 4
            // Major Version equal; Minor Version lower
            List<MavenDependency> inputList4 = new ArrayList<>();
            Version v4Old = new Version("2.18.18", 2,18,"18");
            Version v4New = new Version("2.8.2",2,8,"2");
            MavenCoordinate mc4 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            inputList4.add(mavenDependency4);
            List<MavenDependency> expected4 = new ArrayList<>();

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3),
                    Arguments.of(inputList4, expected4)
            );
        }
    }

    private static class MajorAndMinorSuccessArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version and Minor Version Just in Range
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("43.7.3",43,7,"3");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);

            // Test resources 2
            // Major Version just in Range; Minor Version far in Range
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("43.34.3",43,34,"3");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();
            expected2.add(mavenDependency2);

            // Test resources 3
            // Major Version far ahead; Minor Version not in Range
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("42.2.19", 42,2,"19");
            Version v3New = new Version("48.0.0",48,0,"0");
            MavenCoordinate mc3 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();
            expected3.add(mavenDependency3);

            // Test resources 4
            // Both, Major Version and Minor Version far ahead of Range
            List<MavenDependency> inputList4 = new ArrayList<>();
            Version v4Old = new Version("42.2.19", 42,2,"19");
            Version v4New = new Version("48.26.36",48,26,"36");
            MavenCoordinate mc4 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            inputList4.add(mavenDependency4);
            List<MavenDependency> expected4 = new ArrayList<>();
            expected4.add(mavenDependency4);

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3),
                    Arguments.of(inputList4, expected4)
            );
        }
    }

    private static class MajorAndMinorFailureArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version in Range; Minor Version 1 Version lower than configured
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("43.6.3",43,6,"3");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();

            // Test resources 2
            // Major Version Equal; Minor Version in Range
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("42.14.3",42,14,"3");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();

            // Test resources 3
            // Both, Major Version and Minor Version not in Range
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("42.2.19", 42,2,"19");
            Version v3New = new Version("42.3.0",42,3,"0");
            MavenCoordinate mc3 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();

            // Test resources 4
            // Both, Major Version and Minor Version lower than the old Version
            List<MavenDependency> inputList4 = new ArrayList<>();
            Version v4Old = new Version("42.2.19", 42,2,"19");
            Version v4New = new Version("40.0.0",40,0,"0");
            MavenCoordinate mc4 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            inputList4.add(mavenDependency4);
            List<MavenDependency> expected4 = new ArrayList<>();

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3),
                    Arguments.of(inputList4, expected4)
            );
        }
    }

    private static class ZeroSuccessArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Major Version higher
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("43.2.19",43,2,"19");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);

            // Test resources 2
            // Major Version equal; Minor Version higher
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("42.3.19",42,3,"3");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();
            expected2.add(mavenDependency2);

            // Test resources 3
            // Both, Major Version and Minor Version higher
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("42.2.19", 42,2,"19");
            Version v3New = new Version("44.26.32",44,26,"32");
            MavenCoordinate mc3 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();
            expected3.add(mavenDependency3);

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3)
            );
        }
    }

    private static class ZeroFailureArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // Old Version and New Version are equal
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("42.2.19",42,2,"19");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            inputList1.add(mavenDependency1);
            List<MavenDependency> expected1 = new ArrayList<>();

            // Test resources 2
            // Major Version lower; Minor Version higher
            List<MavenDependency> inputList2 = new ArrayList<>();
            Version v2Old = new Version("42.2.19", 42,2,"19");
            Version v2New = new Version("40.6.19",40,6,"19");
            MavenCoordinate mc2 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList2.add(mavenDependency2);
            List<MavenDependency> expected2 = new ArrayList<>();

            // Test resources 3
            // Major Version equal; Minor Version lower
            List<MavenDependency> inputList3 = new ArrayList<>();
            Version v3Old = new Version("42.2.19", 42,2,"19");
            Version v3New = new Version("42.0.0",42,0,"0");
            MavenCoordinate mc3 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            inputList3.add(mavenDependency3);
            List<MavenDependency> expected3 = new ArrayList<>();

            return Stream.of(
                    Arguments.of(inputList1, expected1),
                    Arguments.of(inputList2, expected2),
                    Arguments.of(inputList3, expected3)
            );
        }
    }

    private static class PropertiesFileSuccessArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // mavenDependency1 id configured differently in the configuration file input1_valid.properties
            // and should therefore added to the output list only if the difference of the versions is in
            // range of the setting
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("48.12.24",48,12,"24");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            Version v2Old = new Version("2.18.18", 2,18,"18");
            Version v2New = new Version("3.18.20",3,18,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList1.add(mavenDependency1);
            inputList1.add(mavenDependency2);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency1);
            expected1.add(mavenDependency2);

            return Stream.of(
                    Arguments.of(inputList1, expected1)
            );
        }
    }

    private static class PropertiesFileFailureArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // Test resources 1
            // mavenDependency1 id configured differently in the configuration file input1_valid.properties
            // and should therefore not added to the output list if the difference of the versions is not in
            // range of the setting
            List<MavenDependency> inputList1 = new ArrayList<>();
            Version v1Old = new Version("42.2.19", 42,2,"19");
            Version v1New = new Version("43.2.19",43,2,"19");
            MavenCoordinate mc1 = new MavenCoordinate("org.postgresql", "postgresql");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            Version v2Old = new Version("2.18.18", 2,18,"18");
            Version v2New = new Version("3.18.20",3,18,"20");
            MavenCoordinate mc2 = new MavenCoordinate("org.projectlombok", "lombok");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            inputList1.add(mavenDependency1);
            inputList1.add(mavenDependency2);
            List<MavenDependency> expected1 = new ArrayList<>();
            expected1.add(mavenDependency2);

            return Stream.of(
                    Arguments.of(inputList1, expected1)
            );
        }
    }
}

