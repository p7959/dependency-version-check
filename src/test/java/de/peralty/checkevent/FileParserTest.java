package de.peralty.checkevent;

import de.peralty.checkevent.config.FilesConfig;
import de.peralty.checkevent.data.MavenCoordinate;
import de.peralty.checkevent.data.MavenDependency;
import de.peralty.checkevent.data.Version;
import de.peralty.checkevent.services.FileParser;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.core.io.PathResource;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class FileParserTest {

    private FileParser fileParser;
    private FilesConfig filesConfig;

    @BeforeEach
    void init() {
        filesConfig = mock(FilesConfig.class);
        fileParser = new FileParser(filesConfig);
    }

    @Nested
    @DisplayName("Correct data input should be handled correctly")
    class SuccessCases {

        @ParameterizedTest
        @ArgumentsSource(DependencyArgumentsProviderNormal.class)
        @DisplayName("Testing if the correct List of MavenDependencies is returned on given normal input")
        void parseFileTestNormalInput(String filePath, List<MavenDependency> expected) throws IOException {
            when(filesConfig.getInput()).thenReturn(new PathResource(filePath));
            List<MavenDependency> provided = fileParser.parseFile();
            assertEquals(expected, provided);
        }

        @ParameterizedTest
        @ArgumentsSource(DependencyArgumentsProviderRandomStrings.class)
        @DisplayName("Testing if the correct List of MavenDependencies is returned if " +
                "the Input File contains random lines of strings")
        void parseFileExtraLinesTest(String filePath, List<MavenDependency> expected) throws IOException {
            when(filesConfig.getInput()).thenReturn(new PathResource(filePath));
            List<MavenDependency> provided = fileParser.parseFile();
            assertEquals(expected, provided);
        }

        @ParameterizedTest
        @ArgumentsSource(DependencyArgumentsProviderUnusual.class)
        @DisplayName("Testing if the correct List of MavenDependencies is returned on given " +
                "Input of Maven dependencies that appear in an unusual format")
        void parseFileUnusualInput(String filePath, List<MavenDependency> expected) throws IOException {
            when(filesConfig.getInput()).thenReturn(new PathResource(filePath));
            List<MavenDependency> provided = fileParser.parseFile();
            assertEquals(expected, provided);
        }
    }

    @Nested
    @DisplayName("Wrong data input should generate a failure")
    class FailureCases {
        @Test
        @DisplayName("Should handle the exception when given input file-path is not found")
        void parseFileNoFileFound() {
            assertThrows(NoSuchFileException.class, () -> {
                String filePath = "wrong-path.txt";
                when(filesConfig.getInput()).thenReturn(new PathResource(filePath));
                fileParser.parseFile();
            });
        }
    }

    private static class DependencyArgumentsProviderNormal implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // expected List 1 (for "testFile1.txt")
            List<MavenDependency> expectedList1 = new ArrayList<>();
            Version v1Old = new Version("7.1.1",7,1,"1");
            Version v1New = new Version("8.0.1",8,0,"1");
            MavenCoordinate mc1 = new MavenCoordinate("com.google.firebase", "firebase-admin");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            Version v2Old = new Version("0.3.0",0,3,"0");
            Version v2New = new Version("1.0.0",1,0,"0");
            MavenCoordinate mc2 = new MavenCoordinate("de.cluetec.logging", "gelf-logging-appender");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            Version v3Old = new Version("1.6.4",1,6,"4");
            Version v3New = new Version("1.7.4",1,7,"4");
            MavenCoordinate mc3 = new MavenCoordinate("io.micrometer", "micrometer-registry-influx");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            expectedList1.add(mavenDependency1);
            expectedList1.add(mavenDependency2);
            expectedList1.add(mavenDependency3);

            // expected List 2 (for "testFile2.txt)
            List<MavenDependency> expectedList2 = new ArrayList<>();
            Version v4Old = new Version("2.11.0",2,11,"0");
            Version v4New = new Version("2.16.0",2,16,"0");
            MavenCoordinate mc4 = new MavenCoordinate("com.drewnoakes", "metadata-extractor");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            Version v5Old = new Version("2.2.2",2,2,"2");
            Version v5New = new Version("2.14.0",2,14,"0");
            MavenCoordinate mc5 = new MavenCoordinate("com.vladmihalcea", "hibernate-types-52");
            MavenDependency mavenDependency5 =
                    new MavenDependency(mc5, v5Old, v5New);
            Version v6Old = new Version("2.0.6",2,0,"6");
            Version v6New = new Version("2.5.6",2,5,"6");
            MavenCoordinate mc6 = new MavenCoordinate("org.springframework.boot",
                    "spring-boot-starter-actuator");
            MavenDependency mavenDependency6 = new MavenDependency(mc6, v6Old, v6New);
            expectedList2.add(mavenDependency4);
            expectedList2.add(mavenDependency5);
            expectedList2.add(mavenDependency6);

            // expected List 3 (for "testFile3.txt)
            List<MavenDependency> expectedList3 = new ArrayList<>();
            Version v7Old = new Version("2.4.3",2,4,"3");
            Version v7New = new Version("2.6.0-RC1",2,6,"0-RC1");
            MavenCoordinate mc7 = new MavenCoordinate("org.springframework.boot",
                    "spring-boot-devtools");
            MavenDependency mavenDependency7 = new MavenDependency(mc7, v7Old, v7New);
            Version v8Old = new Version("3.1.1",3,1,"1");
            Version v8New = new Version("3.2.0-M3",3,2,"0-M3");
            MavenCoordinate mc8 = new MavenCoordinate("org.springframework.cloud",
                    "spring-cloud-stream-binder-rabbit");
            MavenDependency mavenDependency8 = new MavenDependency(mc8, v8Old, v8New);
            expectedList3.add(mavenDependency7);
            expectedList3.add(mavenDependency8);

            // returning Stream of the test values
            return Stream.of(
                    Arguments.of("./src/test/resources/inputFiles/normalInput/testFile1.txt", expectedList1),
                    Arguments.of("./src/test/resources/inputFiles/normalInput/testFile2.txt", expectedList2),
                    Arguments.of("./src/test/resources/inputFiles/normalInput/testFile3.txt", expectedList3)
            );
        }
    }

    private static class DependencyArgumentsProviderRandomStrings implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // expected List 1 (for "testFile4.txt)
            List<MavenDependency> expectedList1 = new ArrayList<>();
            Version v1Old = new Version("2.11.0", 2,11,"0");
            Version v1New = new Version("2.16.0", 2,16,"0");
            MavenCoordinate mc1 = new MavenCoordinate("com.drewnoakes", "metadata-extractor");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            Version v2Old = new Version("2.2.2",2,2,"2");
            Version v2New = new Version("2.14.0",2,14,"0");
            MavenCoordinate mc2 = new MavenCoordinate("com.vladmihalcea", "hibernate-types-52");
            MavenDependency mavenDependency2 =
                    new MavenDependency(mc2, v2Old, v2New);
            Version v3Old = new Version("0.3.0", 0,3,"0");
            Version v3New = new Version("1.0.0",1,0,"0");
            MavenCoordinate mc3 = new MavenCoordinate("de.cluetec.logging", "gelf-logging-appender");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            expectedList1.add(mavenDependency1);
            expectedList1.add(mavenDependency2);
            expectedList1.add(mavenDependency3);

            // returning Stream of the test values
            return Stream.of(
                    Arguments.of("./src/test/resources/inputFiles/randomLineInput/testFile4.txt", expectedList1)
            );
        }
    }

    private static class DependencyArgumentsProviderUnusual implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            // expected List 1 (for "testFile5.txt)
            List<MavenDependency> expectedList1 = new ArrayList<>();
            Version v1Old = new Version("2.3.1",2,3,"1");
            Version v1New =
                    new Version("2.4.0-b180830.0359",2,4,"0-b180830.0359");
            MavenCoordinate mc1 = new MavenCoordinate("javax.xml.bind", "jaxb-api");
            MavenDependency mavenDependency1 =
                    new MavenDependency(mc1, v1Old, v1New);
            expectedList1.add(mavenDependency1);

            // expected List 2 (for "testFile6.txt)
            List<MavenDependency> expectedList2 = new ArrayList<>();
            Version v2Old = new Version("2.0.6.RELEASE",2,0,"6.RELEASE");
            Version v2New = new Version("2.5.6",2,5,"6");
            MavenCoordinate mc2 = new MavenCoordinate("org.springframework.boot",
                    "spring-boot-starter-actuator");
            MavenDependency mavenDependency2 = new MavenDependency(mc2, v2Old, v2New);
            expectedList2.add(mavenDependency2);

            // expected List 3 (for "testFile7.txt)
            List<MavenDependency> expectedList3 = new ArrayList<>();
            Version v3Old = new Version("1.5",1,5,null);
            Version v3New = new Version("1.9.0",1,9,"0");
            MavenCoordinate mc3 = new MavenCoordinate("org.apache.commons", "commons-csv");
            MavenDependency mavenDependency3 =
                    new MavenDependency(mc3, v3Old, v3New);
            expectedList3.add(mavenDependency3);

            // expected List 3 (for "testFile8.txt)
            List<MavenDependency> expectedList4 = new ArrayList<>();
            Version v4Old =
                    new Version("2.6",2,6,null);
            Version v4New =
                    new Version("20030203.000550",20030203, 550,null);
            MavenCoordinate mc4 = new MavenCoordinate("commons-io", "commons-io");
            MavenDependency mavenDependency4 =
                    new MavenDependency(mc4, v4Old, v4New);
            expectedList4.add(mavenDependency4);

            // returning Stream of the test values
            return Stream.of(
                    Arguments.of("./src/test/resources/inputFiles/unusualInput/testFile5.txt", expectedList1),
                    Arguments.of("./src/test/resources/inputFiles/unusualInput/testFile6.txt", expectedList2),
                    Arguments.of("./src/test/resources/inputFiles/unusualInput/testFile7.txt", expectedList3),
                    Arguments.of("./src/test/resources/inputFiles/unusualInput/testFile8.txt", expectedList4)
            );
        }
    }
}
